module "controllers" {
  project-name = var.project-name

  cluster_details = data.terraform_remote_state.aws.outputs

  source = "./modules/controllers/alb-controller"

}



module "app_deployment" {
    source = "./modules/app-deployment"

}

