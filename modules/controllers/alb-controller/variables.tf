variable "project-name" {
  type    = string
  default = ""
}

variable "cluster_details" {
  default = ""
}

variable "region" {
  default = "ap-south-1"
}