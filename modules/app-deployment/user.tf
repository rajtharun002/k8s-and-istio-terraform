resource "kubernetes_service_account" "user-service-account" {
  metadata {
    name      = "user-service-account"
    namespace = "demo"
  }
}

resource "kubernetes_service" "user-service" {
  metadata {
    name      = "user-service"
    namespace = "demo"
  }
  spec {
    selector = {
      app = "user-app"
    }
    type = "ClusterIP"
    port {
      protocol    = "TCP"
      port        = 2002
      target_port = 2002
    }
  }
}

resource "kubernetes_deployment" "user" {
  metadata {
    name      = "user-app"
    namespace = "demo"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app  = "user-app"
        role = "api"
      }
    }
    template {
      metadata {
        labels = {
          app  = "user-app"
          role = "api"
        }
      }
      spec {
        container {
          name  = "user-container"
          image = "249248043983.dkr.ecr.ap-south-1.amazonaws.com/user-service"
          resources {
            limits = {
              memory = "128Mi"
              cpu    = "500m"
            }
          }
          port {
            container_port = 2002
          }
        }
      }
    }
  }
}
