resource "kubernetes_service_account" "booking-service-account" {
  metadata {
    name      = "booking-service-account"
    namespace = "demo"
  }
}

resource "kubernetes_service" "booking-service" {
  metadata {
    name      = "booking-service"
    namespace = "demo"
  }
  spec {
    selector = {
      app = "booking-app"
    }
    type = "ClusterIP"
    port {
      protocol    = "TCP"
      port        = 2001
      target_port = 2001
    }
  }
}

resource "kubernetes_deployment" "booking" {
  metadata {
    name      = "booking-deployment"
    namespace = "demo"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app  = "booking-app"
        role = "api"
      }
    }
    template {
      metadata {
        labels = {
          app  = "booking-app"
          role = "api"
        }
      }
      spec {
        container {
          name  = "booking-container"
          image = "249248043983.dkr.ecr.ap-south-1.amazonaws.com/booking-service"
          resources {
            limits = {
              memory = "128Mi"
              cpu    = "500m"
            }
          }
          port {
            container_port = 2001
          }
        }
      }
    }
  }
}
