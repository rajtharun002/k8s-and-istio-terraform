resource "kubernetes_namespace" "demo" {
  metadata {
    name = "demo"

    annotations = {
      "istio-injection" = "enabled"
    }
  }
}

data "kubectl_file_documents" "ingress" {
  content = file("./modules/istio/ingress.yaml")
}

resource "kubectl_manifest" "ingress-class" {
  depends_on = [
    kubernetes_namespace.demo
  ]
  for_each  = data.kubectl_file_documents.ingress.manifests
  yaml_body = each.value
}

data "kubectl_file_documents" "gateway" {
  content = file("./modules/istio/gateway.yaml")
}

resource "kubectl_manifest" "gateway" {
  depends_on = [
    kubernetes_namespace.demo
  ]
  for_each  = data.kubectl_file_documents.gateway.manifests
  yaml_body = each.value
}

data "kubectl_file_documents" "virtual-service" {
  content = file("./modules/istio/virtual-service.yaml")
}

resource "kubectl_manifest" "virtual-service" {
  depends_on = [
    kubernetes_namespace.demo
  ]
  for_each  = data.kubectl_file_documents.virtual-service.manifests
  yaml_body = each.value
}

data "kubectl_file_documents" "secret" {
  content = file("./modules/istio/secret.yaml")
}

resource "kubectl_manifest" "secret" {
  depends_on = [
    kubernetes_namespace.demo
  ]
  for_each  = data.kubectl_file_documents.secret.manifests
  yaml_body = each.value
}

data "kubectl_file_documents" "bluegreen-app-rule" {
  content = file("./modules/istio/bluegreen/bluegreen-rule.yaml")
}

resource "kubectl_manifest" "bluegreen-app-rule" {
  depends_on = [
    kubernetes_namespace.demo
  ]
  for_each  = data.kubectl_file_documents.bluegreen-app-rule.manifests
  yaml_body = each.value
}

data "kubectl_file_documents" "bluegreen-app-vservice" {
  content = file("./modules/istio/bluegreen/bluegreen-vservice.yaml")
}

resource "kubectl_manifest" "bluegreen-app-vservice" {
  depends_on = [
    kubernetes_namespace.demo
  ]
  for_each  = data.kubectl_file_documents.bluegreen-app-vservice.manifests
  yaml_body = each.value
}

data "kubectl_file_documents" "bluegreen-app" {
  content = file("./modules/istio/bluegreen/bluegreen-app.yaml")
}

resource "kubectl_manifest" "bluegreen-app" {
  depends_on = [
    kubernetes_namespace.demo
  ]
  for_each  = data.kubectl_file_documents.bluegreen-app.manifests
  yaml_body = each.value
}
