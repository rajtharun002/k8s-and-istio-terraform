terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.17"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.8"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }
  }
}

data "terraform_remote_state" "aws" {
  backend = "local"

  config = {
    path = "../sutherland_poc_terraform/terraform.tfstate"
  }
}


data "aws_eks_cluster" "cluster" {
  name = data.terraform_remote_state.aws.outputs.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = data.terraform_remote_state.aws.outputs.cluster_id
}


provider "aws" {
  region  = var.region
  profile = var.profile
}

provider "helm" {
  kubernetes {
    host                   = data.terraform_remote_state.aws.outputs.cluster_endpoint
    cluster_ca_certificate = base64decode(data.terraform_remote_state.aws.outputs.cluster_certificate_authority_data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
}

# Terraform Kubernetes Provider
provider "kubernetes" {
  host                   = data.terraform_remote_state.aws.outputs.cluster_endpoint
  cluster_ca_certificate = base64decode(data.terraform_remote_state.aws.outputs.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

provider "http" {
  # Configuration options
}

provider "kubectl" {
  host                   = data.terraform_remote_state.aws.outputs.cluster_endpoint
  cluster_ca_certificate = base64decode(data.terraform_remote_state.aws.outputs.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
}
