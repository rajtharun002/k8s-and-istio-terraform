variable "project-name" {
  description = "Project Name"
  type        = string
  default     = "multicoreware-poc"
}

variable "region" {
  description = "Project infra region"
  type        = string
  default     = "ap-south-1"
}

variable "profile" {
  description = "Aws secret key and access key profile"
  type        = string
  default     = "multicoreware-poc"
}
